function drawCategories(categories) {
  for (var i = 0; i < categories.length; i++) {
    const categoryTemplate =
            '<div class="category" data-category-id="'
            + categories[i].id
            + '">'
            + categories[i].name
            + '</div>';
    $('.categories').append(categoryTemplate);
  }
}

function drawNews(newsList, categoryId, page) {
  for (var i = 0; i < newsList.length; i++) {
    const currentNewsItem = newsList[i];
    const newsDate = new Date(currentNewsItem.date);
    const newsTemplate =
            '<div class="news_item" data-news-id="'
            + currentNewsItem.id
            + '"><p>'
            + currentNewsItem.title
            + '</p><p>'
            + newsDate.toLocaleString()
            + '</p><p>'
            + currentNewsItem.shortDescription
            + '</div>';
    $('.news').append(newsTemplate);
  }

  if (newsList.length === 0) {
    if (page === 0) {
      $('.news').append('<div class="no_news">Здесь нет новостей.</div>');
    } else {
      $('.news').append('<div class="no_news">Новости закончились.</div>');
    }
  } else {
    $('.news').append('<div class="pagination"/>');
    if (page === 0) {
      $('.news .pagination').append('<span class="page1">1</span><span class="space"/><a href="#" class="page2">2</a>');
    } else {
      $('.news .pagination').append('<a href="#" class="page1">1</a><span class="space"/><span class="page2">2</span>');
    }
    $('.news .pagination a.page1').click(function() {
      loadAndDisplayCategoryNews(categoryId, 0);
      return false;
    });
    $('.news .pagination a.page2').click(function() {
      loadAndDisplayCategoryNews(categoryId, 1);
      return false;
    });
  }
};

function drawFullNewsItem(newsItem) {
  const newsText = newsItem.title + '<br>' + newsItem.date + '<br>' + newsItem.fullDescription;
  $('.full_news').append(newsText);
};

function onNewsItemClick () {
  const newsId = $(this).data("newsId");

  const newsItemURL = 'http://testtask.sebbia.com/v1/news/details?id=' + newsId;
  $.getJSON(newsItemURL, function (data) {
    $('.news').css({'display':'none'});
    $('.full_news').css({'display':'block'});

    const newsItem = data.news;
    drawFullNewsItem(newsItem);
  });
}

function loadAndDisplayCategoryNews(categoryId, page) {
  const categoryNewsURL = 'http://testtask.sebbia.com/v1/news/categories/' + categoryId + '/news?page=' + page;

  $.getJSON(categoryNewsURL, function (data) {
    $('.categories').css({'display':'none'});

    $('.news').empty();
    $('.news').css({'display':'block'});

    const newsList = data.list;
    drawNews(newsList, categoryId, page);

    $('.news .news_item').click(onNewsItemClick);
  });
}

function onCategoryClick() {
  const categoryId = $(this).data("categoryId");
  loadAndDisplayCategoryNews(categoryId, 0);
}


function startApp(){  
  $.getJSON('http://testtask.sebbia.com/v1/news/categories', function (data) {
    const categories = data.list;
    drawCategories(categories);
    $('.category').click(onCategoryClick);
  });
}

$(document).ready(startApp);
